from postgresql_db import Base
from sqlalchemy import Column, String
from postgresql_db.inmutables import _Params


class User(Base):
    __tablename__ = 'user'

    uuid = Column(String, primary_key=True)
    username = Column(String)
    email = Column(String)
    lastname = Column(String)

    def __init__(self, **kwargs):
        self.uuid = kwargs.get(_Params.UUID)
        self.username = kwargs.get(_Params.USERNAME)
        self.email = kwargs.get(_Params.EMAIL)
        self.lastname = kwargs.get(_Params.LASTNAME)

